const path = require('path');
const http = require('http');
const express = require('express');
const socketIO = require('socket.io');
const { generateMessage } = require('./utils/message');
const PORT = process.env.PORT || 3000;

const publicPath = path.join(__dirname, "../public");
const app = express();
var server = http.createServer(app);
var io = socketIO(server);
app.use(express.static(publicPath));

io.on('connection', (socket) => {
    console.log('New user connected');

    socket.on('createMessage', (msg, callback) => {
        console.log('create message :', msg);
        io.emit('newMessage', generateMessage(msg.from,msg.text));
        callback('This is ack from server');
    });

    socket.on('disconnect', () => {
        console.log('New user disconnected');
    });
});


server.listen(PORT, () => {
    console.log('server is up on port :' + PORT);
})