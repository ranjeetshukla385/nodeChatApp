var socket = io();
socket.on('connect', () => {
    console.log('connected to server');
});
socket.on('disconnect', () => {
    console.log('Disconnected from server');
});
socket.on('newMessage', function (msg) {
    var li = $('<li></li>');
    li.text(`${msg.from}: ${msg.text}`);

    $('#messages').append(li);
    console.log('New meassge :', msg);
});

$(document).ready(function () {
    $('#message-form').on('submit', function (e) {
        e.preventDefault();
        socket.emit('createMessage', {
            from: 'Ranjeet',
            text: $('#message').val()
        }, function (data) {
            console.log('Response : ' + data);
        });
    });
});
